#!/usr/bin/env python3
#
# Make sure Selenium is running:
# > cd selenium-firefox && sudo docker-compose up -d

import argparse
import getpass
import json
import logging
import os
import sys

from contextlib import contextmanager
from datetime import datetime

from selenium import webdriver
from selenium.common.exceptions import (
    NoSuchElementException,
    InvalidCookieDomainException,
)
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import (
    ui,
    expected_conditions as expconds
)
from statemachine import (
    StateMachine,
    State
)



logging.basicConfig(
    format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%Y-%m-%d:%H:%M:%S'
)

logger = logging.getLogger(__name__)



@contextmanager
def wait_for_page(browser, donefn=None, msg=None, timeout=60, pollfreq=5):
    oldpage = browser.find_element(By.XPATH, '//html')
    yield

    def has_update(driver):
        logger.info('Checking for page update ...')
        is_stale = expconds.staleness_of(oldpage)
        return is_stale(driver)

    try:
        ui.WebDriverWait(browser, timeout, pollfreq).until(
            has_update, 'No page update detected'
        )
    except Exception as ex:
        screener = Screenshotter(browser)
        sourcer = Sourcer(browser)
        screener.save('ff-error.png')
        sourcer.save('ff-error.html')
        raise ex

def wait_for(browser, donefn, msg, timeout=60, pollfreq=5):
    element = ui.WebDriverWait(browser, timeout, pollfreq).until(
        donefn, msg
    )
    return element



def initialize_browser():
    dc = webdriver.DesiredCapabilities.FIREFOX
    dc['loggingPrefs'] = { 'browser': 'ALL' }
    browser = webdriver.Remote(
        command_executor='http://localhost:4444/wd/hub',
        desired_capabilities=dc
    )
    return browser



RUNID = datetime.now().strftime('%Y%m%d-%H:%M:%S-%f')

class Screenshotter():
    def __init__(self, browser, dirpath='debug/screenshot', runid=RUNID):
        self.browser = browser
        self.prefix = os.path.join(dirpath, runid)

    def save(self, filename):
        os.makedirs(self.prefix, exist_ok=True)
        path = os.path.join(self.prefix, filename)
        self.browser.save_screenshot(path)

class Sourcer():
    TIME_FORMAT = '%Y%m%d-%H:%M:%S-%f'

    def __init__(self, browser, dirpath='debug/html', runid=RUNID):
        self.browser = browser
        self.prefix = os.path.join(dirpath, runid)

    def save(self, filename):
        os.makedirs(self.prefix, exist_ok=True)
        html = self.browser.page_source
        path = os.path.join(self.prefix, filename)
        with open(path, 'w') as ff:
            ff.write(html)



class LoginStateMachine(StateMachine):
    at_setup = State('setup', initial=True)
    at_username = State('username')
    at_password = State('password')
    at_2fa = State('2fa')
    at_pick = State('pick-account')
    at_done = State('done')

    to_username = at_setup.to(at_username)
    to_password = at_username.to(at_password)
    to_2fa = at_password.to(at_2fa)
    to_pick = at_2fa.to(at_pick)
    to_done = at_done.from_(at_setup, at_pick)

    def __init__(self,
        browser,
        username,
        password,
    ):
        super().__init__(self)

        self.browser = browser
        self.username = username
        self.password = password
        self.screenshotter = Screenshotter(browser)
        self.sourcer = Sourcer(browser)

    def maybe_solve_recaptcha(self, browser):
        ''' Handle ReCAPTCHA if necessary '''
        screener = self.screenshotter
        sourcer = self.sourcer

        try:
            logger.info('Detecting ReCAPTCHA...')
            recaptcha_frame = browser.find_element_by_xpath('//*[@id="g-recaptcha"]//iframe')
            browser.switch_to_frame(recaptcha_frame)
            screener.save('z-recaptcha-1.png')
            recaptcha_anchor = browser.find_element_by_xpath('//*[@id="recaptcha-anchor"]')
            recaptcha_anchor.click()

            def checkbox_marked(driver):
                logger.info('Waiting for ReCAPTCHA checkbox ...')
                has_checkmark = expconds.visibility_of_element_located(
                    (By.CLASS_NAME, 'recaptcha-checkbox-checkmark')
                )
                return has_checkmark(driver)
            wait_for(browser, checkbox_marked, 'ReCAPTCHA could not be marked')

            browser.switch_to_default_content()

            screener.save('z-recaptcha-2.png')
            commit_button = browser.find_element_by_xpath('//button[@name="commit"]')

            with wait_for_page(browser):
                # Calling .click() directly causes ElementClickInterceptedException
                # from parent ul that does not happen when manually clicking
                ( ActionChains(browser)
                  .move_to_element(commit_button)
                  .click(commit_button)
                  .perform()
                )

            return
        except NoSuchElementException:
            screener.save('z-recaptcha-0.png')
            logger.info('ReCAPTCHA not present')
            return
        except Exception as ex:
            sourcer.save('z-recaptcha-f.html')
            screener.save('z-recaptcha-f.png')
            raise ex

    def login(self, store):
        browser = self.browser
        sourcer = self.sourcer

        # Navigate to admin page and wait for redirection
        admin_url = 'https://{}/admin'.format(store)
        logger.info('Requesting {}'.format(admin_url))
        browser.get(admin_url)

        # Wait for window.location redirect
        at_shopify = expconds.title_contains('Shopify')
        def until_at_shopify(driver):
            if at_shopify(driver):
                logger.info(
                    f'Redirected by window.location to {driver.current_url}')
                return True
            logger.info('Waiting for window.location redirect')
            sourcer.save('1-redirect.html')
            return False
        wait_for(browser,
            until_at_shopify, 'Failed to be redirected by window.location'
        )

        # Wait for turbolinks to refresh page
        body_present = expconds.presence_of_element_located((By.TAG_NAME, 'body'))
        def until_body_present(driver):
            if body_present(driver):
                logger.info(f'Turbolinks loaded html body')
                return True
            logger.info('Waiting for turbolinks to load html body')
            sourcer.save('2-turbolinks.html')
            return False
        wait_for(browser,
            until_body_present, 'Turbolinks failed to load html body'
        )

        if browser.current_url.startswith(admin_url):
            logger.info('Successfully logged in with cookies')
            self.to_done()
            return

        logger.info('Redirected to {}'.format(browser.current_url))
        self.to_username()
        return

    def on_enter_at_username(self):
        browser = self.browser
        screener = self.screenshotter
        sourcer = self.sourcer
        username = self.username

        # Wait for input to exist
        email_input_xp = (By.XPATH, '//input[@id="account_email"]')
        def has_email(driver):
            logger.info('Waiting for email input ...')
            is_there = expconds.presence_of_element_located(
                email_input_xp
            )
            return is_there(driver)
        email_input = wait_for(browser, has_email, 'Email input not found')
        screener.save('1-email-a.png')
        sourcer.save('3-email-a.html')

        if (
            email_input.get_attribute('readonly') and
            email_input.get_property('value') == username
        ):
            logger.info(f'Email input already "{username}"')
            self.to_password()

        # Wait for input to become writable
        def can_write(driver):
            logger.info('Waiting for email input to be writable ...')
            write_ready = expconds.element_to_be_clickable(
                email_input_xp
            )
            return write_ready(driver)
        email_input = wait_for(browser, can_write, 'Email input not writable')

        # Focus on email input
        email_input.clear()

        email_submit_xp = (By.XPATH, '//button[@name="commit"]')
        def can_submit(driver):
            logger.info('Waiting for email submit to be clickable')
            submit_ready = expconds.element_to_be_clickable(
                email_submit_xp
            )
            return submit_ready(driver)
        email_submit = wait_for(browser,
            can_submit, 'Email submit not clickable')

        screener.save('1-email-b.png')
        sourcer.save('3-email-b.html')

        logger.info('Submitting email...')
        email_input.send_keys(username)
        email_submit.click()
        sourcer.save('3-email-c.html')
        screener.save('1-email-c.png')

        pollix = 0
        def ready_for_pw(driver):
            sourcer.save(f'3-email-d-{pollix}.html')
            screener.save(f'1-email-d-{pollix}.png')

            is_busy = expconds.presence_of_element_located(
                (By.XPATH, '//html[@class="fresh-html"]')
            )
            return is_busy(driver)

        wait_for(browser, ready_for_pw, 'Password input not found')

        self.to_password()

    def on_enter_at_password(self):
        browser = self.browser
        screener = self.screenshotter
        sourcer = self.sourcer
        password = self.password

        # Password
        sourcer.save('4-password-a.html')
        screener.save('2-password-a.png')

        pw_input_xp = (By.XPATH, '//input[@id="account_password"]')
        def can_write(driver):
            logger.info('Waiting for password input to be writable ...')
            write_ready = expconds.element_to_be_clickable(
                pw_input_xp
            )
            return write_ready(driver)
        pw_input = wait_for(browser, can_write, 'Password input not writable')

        pw_input.clear()

        pw_submit_xp = (By.XPATH, '//button[@name="commit"]')
        def can_submit(driver):
            logger.info('Waiting for password submit to be clickable')
            submit_ready = expconds.element_to_be_clickable(
                pw_submit_xp
            )
            return submit_ready(driver)
        pw_submit = wait_for(browser,
            can_submit, 'Password submit not clickable')

        sourcer.save('4-password-b.html')
        screener.save('2-password-b.png')

        logger.info('Submitting password...')
        pw_input.send_keys(password)
        pw_submit.click()
        sourcer.save('4-password-c.html')
        screener.save('2-password-c.png')

        pollix = 0
        def ready_for_2fa(driver):
            sourcer.save(f'4-password-d-{pollix}.html')
            screener.save(f'2-password-d-{pollix}.png')
            has_2fa = expconds.element_to_be_clickable(
                (By.XPATH, '//input[@id="account_tfa_code"]')
            )
            return has_2fa(driver)

        wait_for(browser, ready_for_2fa, '2fa input not found')

        self.to_2fa()

    def on_enter_at_2fa(self):
        browser = self.browser
        screener = self.screenshotter
        sourcer = self.sourcer

        self.maybe_solve_recaptcha(browser)

        # Two-factor authentication
        sourcer.save('5-2fa-a.html')
        screener.save('3-2fa-a.png')

        tfa_input_xp = (By.XPATH, '//input[@id="account_tfa_code"]')
        def can_write(driver):
            logger.info('Waiting for 2fa input to be writable ...')
            write_ready = expconds.element_to_be_clickable(
                tfa_input_xp
            )
            return write_ready(driver)
        tfa_input = wait_for(browser, can_write, '2fa input not writable')

        tfa_code = input('Enter 2fa code for Shopify: ')
        tfa_input.clear()
        tfa_input.send_keys(tfa_code)

        tfa_submit_xp = (By.XPATH, '//button[@name="commit"]')
        def can_submit(driver):
            logger.info('Waiting for 2fa submit to be clickable')
            submit_ready = expconds.element_to_be_clickable(
                tfa_submit_xp
            )
            return submit_ready(driver)
        tfa_submit = wait_for(browser, can_submit, '2fa submit not clickable')

        with wait_for_page(browser):
            sourcer.save('5-2fa-b.html')
            screener.save('3-2fa-b.png')
            tfa_submit.click()

        self.to_pick()

    def on_enter_at_pick(self):
        browser = self.browser
        screener = self.screenshotter
        sourcer = self.sourcer
        username = self.username

        # Pick account in SSO list
        try:
            screener.save('4-pick.png')
            sourcer.save('6-pick.html')
            acct_xp = (
                '//a['
                    'contains(@class, "account-picker__item") and '
                   f'contains(.//div, "{username}")'
                ']'
            )
            acct_an = browser.find_element_by_xpath(acct_xp)

            with wait_for_page(browser):
                logger.info(f'Picking account "{username}"')
                ( ActionChains(browser)
                    .move_to_element(acct_an)
                    .click(acct_an)
                    .perform()
                )

            self.to_done()

        except NoSuchElementException as ex:
            logger.info('Skip picking account')
            self.to_done()
        except Exception as ex:
            screener.save('4-pick-f.png')
            sourcer.save('6-pick-f.html')
            raise ex

    def on_enter_at_done(self):
        browser = self.browser
        screener = self.screenshotter
        sourcer = self.sourcer

        try:
            screener.save('5-loading.png')
            sourcer.save('7-loading.html')

            logger.info('Waiting for application to load ...')
            app_frame_xp = (By.ID, 'AppFrameMain')
            def app_created(driver):
                has_app = expconds.presence_of_element_located(
                    app_frame_xp
                )
                return has_app(driver)
            wait_for(browser, app_created,
                f'No element found by {app_frame_xp} at {browser.current_url}'
            )

            logger.info('Waiting for application to respond ...')
            def app_ready(driver):
                can_interact = expconds.element_to_be_clickable(
                    app_frame_xp
                )
                return can_interact(driver)
            wait_for(browser, app_ready, 'Application not responding')

        except Exception as ex:
            sourcer.save('8-failed.html')
            screener.save('6-failed.png')
            logger.error('Application failed to load')
            logger.error(ex)
            return

        screener.save('6-done.png')
        sourcer.save('8-done.html')
        logger.info(f'Logged in to {browser.current_url}')



def login(browser, cache):
    store = cache.get('store')
    username = cache.get('username')
    password = cache.get('password')
    cookies = cache.get('cookies')

    # Set cookies if available
    #
    # Note: Cookies can only be set on patched version of Firefox
    #       See selenium-firefox/Dockerfile for explanation
    if cookies:
        logger.info('Adding {} cookies'.format(len(cookies)))
        for cookie in cookies:
            try:
                logger.info('Cookie: {}'.format(cookie))
                browser.add_cookie(cookie)
            except InvalidCookieDomainException as ex:
                logger.info('Invalid domain for cookie {}'.format(cookie))
                pass

    state = LoginStateMachine(
        browser,
        username,
        password,
    )
    state.login(store)

    if not state.is_at_done:
        raise Exception('Login failed unexpectedly')

    # Save cookies for future sessions
    # Even continuing an existing session might refresh tokens
    return browser.get_cookies()



def logout(browser):
    screener = Screenshotter(browser)
    sourcer = Sourcer(browser)

    logger.info("Logging out ...")

    try:
        profile_btn_xp = (By.XPATH,
            '//*[@id="AppFrameTopBar"]'
            '//button[@aria-controls="Polarispopover1"]'
        )
        profile_btn = browser.find_element(*profile_btn_xp)
        if profile_btn:
            logger.info('Clicking on User Profile button')
        ( ActionChains(browser)
            .move_to_element(profile_btn)
            .click(profile_btn)
            .perform()
        )
        def popover_visible(driver):
            is_visible = (
                expconds.visibility_of_element_located(profile_btn_xp)
            )
            return is_visible(driver)
        wait_for(browser, popover_visible, 'Profile popover is not visible')
    except Exception as ex:
        sourcer.save('zz-logout-f.html')
        screener.save('zz-logout-f.png')
        logger.error('Cannot find profile button')
        logger.error(ex)
        browser.quit()
        return

    try:
        logout_btn_xp = (By.XPATH,
            '//*[@id="Polarispopover1"]'
            '//*[contains(text(), "Log out")]'
            '/ancestor::button'
        )
        logout_btn = browser.find_element(*logout_btn_xp)
        if logout_btn:
            logger.info('Clicking on "Log out" button')
        ActionChains(browser).move_to_element(logout_btn).perform()
        sourcer.save('zz-logout-0.html')
        screener.save('zz-logout-0.png')

        def logged_out(driver):
            not_here = (
                expconds.invisibility_of_element(logout_btn)
            )
            return not_here(driver)
        ActionChains(browser).click(logout_btn).perform()
        wait_for(browser, logged_out, 'Log out not working')
        sourcer.save('zz-logout-1.html')
        screener.save('zz-logout-1.png')
    except Exception as ex:
        sourcer.save('zz-logout-f.html')
        screener.save('zz-logout-f.png')
        logger.error('Cannot find logout button')
        logger.error(ex)
        browser.quit()
        return

    # Logged out
    logger.info('Title: {}'.format(browser.title))
    logger.info('Logged out successfully')

    return True



def read_cache(path):
    cache_dict = {}
    try:
        with open(path) as cf:
            cache_dict = json.load(cf)
    except FileNotFoundError as ex:
        logger.warning(ex)
    return cache_dict

def save_cache(path, cache_dict):
    dirpath = os.path.dirname(path)
    if dirpath and not os.path.exists(dirpath):
        os.makedirs(dirpath)

    with open(path, 'w') as ff:
        json.dump(cache_dict, ff, indent=2)

def fetch_credentials(cache_path):
    cache = read_cache(cache_path)
    try:
        store = cache.get('store') or input("Store address: ")
        cache['store'] = store
        save_cache(cache_path, cache)
    except ValueError as ex:
        pass

    try:
        username = cache.get('username') or input('Username: ')
        cache['username'] = username
        save_cache(cache_path, cache)
    except ValueError as ex:
        pass

    try:
        password = cache.get('password') or getpass.getpass()
        cache['password'] = password
        save_cache(cache_path, cache)
    except ValueError as ex:
        pass

    return cache

def savefile(path, content):
    logger.info('Writing {} bytes to {}'.format(len(content), path))

    dirpath = os.path.dirname(path)
    if dirpath and not os.path.exists(dirpath):
        os.makedirs(dirpath)

    with open(path, 'w') as ff:
        wbytes = ff.write(content)
        logger.info('Wrote {} bytes'.format(wbytes))

class SMSTemplate(object):
    URL_FMT = '{webroot}/sms_templates/{webname}/edit'
    FILENAME_FMT = '{fileroot}/{filepfx}-{filesfx}'

    BODY_ID = 'sms_template_body'
    BODY_FILE_SUFFIX = 'sms.liquid'

    def __init__(self, webname, filepfx):
        self.webroot = None
        self.webname = webname
        self.fileroot = None
        self.filepfx = filepfx

    def pull(self, browser):
        if not self.webroot:
            raise AttributeError('webroot must be set before pull()')
        if not self.fileroot:
            logger.warning('fileroot not set. Default to %s', DEFAULT_FILEROOT)
            self.fileroot = DEFAULT_FILEROOT

        remote_url = self.URL_FMT.format(
            webroot=self.webroot,
            webname=self.webname
        )
        logger.info('Fetching %s', remote_url)
        browser.get(remote_url)

        try:
            bodyid = self.BODY_ID
            url = browser.current_url
            def has_body(driver):
                logger.info(f'Waiting for element #{bodyid} ...')
                body_found = expconds.presence_of_element_located(
                    (By.ID, bodyid)
                )
                return body_found(driver)
            el = wait_for(browser, has_body,
                f'No element with id {bodyid} found at {url}'
            )
        except Exception as ex:
            logger.info('html:\n{}'.format(browser.page_source))
            logger.error(ex)
            return

        body_content = el.get_attribute('textContent')
        body_filename = self.FILENAME_FMT.format(
            fileroot=self.fileroot,
            filepfx=self.filepfx,
            filesfx=self.BODY_FILE_SUFFIX
        )
        savefile(body_filename, body_content)

        return True

    def __str__(self):
        return 'sms({})'.format(self.filepfx)



class EmailTemplate(object):
    URL_FMT = '{webroot}/email_templates/{webname}/edit'
    FILENAME_FMT = '{fileroot}/{filepfx}-{filesfx}'

    SUBJECT_ID = 'email_template_title'
    SUBJECT_FILE_SUFFIX = 'email-subject.liquid'
    BODY_ID = 'email_template_body_html'
    BODY_FILE_SUFFIX = 'email-body.liquid'

    def __init__(self, webname, filepfx):
        self.webroot = None
        self.webname = webname
        self.fileroot = None
        self.filepfx = filepfx

    def pull(self, browser):
        if not self.webroot:
            raise AttributeError('webroot must be set before pull()')
        if not self.fileroot:
            logger.warning('fileroot not set. Default to %s', DEFAULT_FILEROOT)
            self.fileroot = DEFAULT_FILEROOT

        remote_url = self.URL_FMT.format(
            webroot=self.webroot,
            webname=self.webname
        )
        logger.info('Fetching %s', remote_url)
        browser.get(remote_url)

        try:
            subjid = self.SUBJECT_ID
            url = browser.current_url
            def has_subject(driver):
                logger.info(f'Waiting for element #{subjid} ...')
                subject_found = expconds.presence_of_element_located(
                    (By.ID, subjid)
                )
                return subject_found(driver)
            el = wait_for(browser, has_subject,
                f'No element with id {subjid} found at {url}'
            )
        except Exception as ex:
            logger.info('html:\n{}'.format(browser.page_source))
            logger.error(ex)
            return

        subject_content = el.get_attribute('value')
        subject_filename = self.FILENAME_FMT.format(
            fileroot=self.fileroot,
            filepfx=self.filepfx,
            filesfx=self.SUBJECT_FILE_SUFFIX
        )
        savefile(subject_filename, subject_content)

        try:
            bodyid = self.BODY_ID
            url = browser.current_url
            def has_body(driver):
                logger.info(f'Waiting for element #{bodyid} ...')
                body_found = expconds.presence_of_element_located(
                    (By.ID, bodyid)
                )
                return body_found(driver)
            el = wait_for(browser, has_body,
                f'No element with id {bodyid} found at {url}'
            )
        except Exception as ex:
            logger.info('html:\n{}'.format(browser.page_source))
            logger.error(ex)
            return

        body_content = el.get_attribute('textContent')
        body_filename = self.FILENAME_FMT.format(
            fileroot=self.fileroot,
            filepfx=self.filepfx,
            filesfx=self.BODY_FILE_SUFFIX
        )
        savefile(body_filename, body_content)

        return True

    def __str__(self):
        return 'email({})'.format(self.filepfx)



def process_requested_notifications():
    notifications = []

    # Note: These templates always inject a newline at EOF
    #
    # SMS
    #   orders/pos-exchange-receipt
    #
    # Email
    #   shipping/fulfillment-request
    #   staff/new-order

    # sms
    notifications.extend([
        SMSTemplate('order_confirmation',          'orders/order-confirmation'),
        SMSTemplate('order_cancelled',             'orders/order-cancelled'),
        SMSTemplate('refund_notification',         'orders/order-refund'),
        SMSTemplate('store_receipt',               'orders/store-receipt'),
        SMSTemplate('pos_exchange_receipt',        'orders/pos-exchange-receipt'),
        SMSTemplate('gift_card_notification',      'orders/gift-card'),
        SMSTemplate('shipping_confirmation',       'shipping/shipping-confirmation'),
        SMSTemplate('shipping_update',             'shipping/shipping-update'),
        SMSTemplate('local_out_for_delivery',      'delivery/local-delivering'),
        SMSTemplate('local_delivered',             'delivery/local-delivered'),
        SMSTemplate('local_missed_delivery',       'delivery/local-missed'),
        SMSTemplate('ready_for_pickup',            'delivery/pickup-ready'),
        SMSTemplate('pickup_receipt',              'delivery/pickup-done'),
    ])

    # email
    notifications.extend([
        EmailTemplate('order_confirmation',                'orders/order-confirmation'),
        EmailTemplate('order_edited',                      'orders/order-edit'),
        EmailTemplate('order_edit_invoice',                'orders/order-edit-invoice'),
        EmailTemplate('order_invoice',                     'orders/order-invoice'),
        EmailTemplate('order_cancelled',                   'orders/order-cancelled'),
        EmailTemplate('refund_notification',               'orders/order-refund'),
        EmailTemplate('draft_order_invoice',               'orders/draft-order-invoice'),
        EmailTemplate('buy_online',                        'orders/buy-online'),
        EmailTemplate('abandoned_checkout_notification',   'orders/abandoned-checkout'),
        EmailTemplate('store_receipt',                     'orders/store-receipt'),
        EmailTemplate('pos_exchange_receipt',              'orders/pos-exchange-receipt'),
        EmailTemplate('gift_card_notification',            'orders/gift-card'),
        EmailTemplate('failed_payment_processing',         'orders/failed-payment'),
        EmailTemplate('fulfillment_request',               'shipping/fulfillment-request'),
        EmailTemplate('shipping_confirmation',             'shipping/shipping-confirmation'),
        EmailTemplate('shipping_update',                   'shipping/shipping-update'),
        EmailTemplate('shipment_out_for_delivery',         'delivery/shipment-delivering'),
        EmailTemplate('shipment_delivered',                'delivery/shipment-delivered'),
        EmailTemplate('local_out_for_delivery',            'delivery/local-delivering'),
        EmailTemplate('local_delivered',                   'delivery/local-delivered'),
        EmailTemplate('local_missed_delivery',             'delivery/local-missed'),
        EmailTemplate('ready_for_pickup',                  'delivery/pickup-ready'),
        EmailTemplate('pickup_receipt',                    'delivery/pickup-done'),
        EmailTemplate('customer_account_activate',         'customer/account-invite'),
        EmailTemplate('customer_account_welcome',          'customer/account-welcome'),
        EmailTemplate('customer_account_reset',            'customer/account-reset'),
        EmailTemplate('customer_update_payment_method',    'customer/payment-update'),
        EmailTemplate('contact_buyer',                     'customer/contact-buyer'),
        EmailTemplate('customer_marketing_confirmation',   'customer/subscription-signup'),
        EmailTemplate('return_created',                    'shipping/return-created'),
        EmailTemplate('return_label_notification',         'shipping/return-label'),
        EmailTemplate('new_order_notification',            'staff/new-order'),
    ])

    return notifications

def pull_notifications(browser, cache, fileroot):
    store = cache.get('store')
    webroot = 'https://{}/admin'.format(store)

    notifications = process_requested_notifications()

    failed = []
    for notification in notifications:
        notification.webroot = webroot
        notification.fileroot = fileroot
        if not notification.pull(browser):
            failed.append(notification)

    if not len(failed):
        return

    for notification in failed:
        logger.warning('Could not fetch %s', notification)



def main(args):
    cache_path = args.cache_path
    cache = fetch_credentials(cache_path)
    browser = initialize_browser()

    # Log in and save cookies for future sessions
    cookies = login(browser, cache)
    if cookies:
        logger.info(f'Saving cookies to {cache_path}')
        cache['cookies'] = cookies
        save_cache(cache_path, cache)

    pull_notifications(browser, cache, args.file_root)

    # Log out and delete cookies
    logged_out = False
    if not args.no_logout:
        logged_out = logout(browser)
    if logged_out:
        logger.info(f'Clearing cookies from {cache_path}')
        cache['cookies'] = []
        save_cache(cache_path, cache)


def configure_logger(args):
    if args.verbose > 3:
        logger.level = logging.DEBUG
    elif args.verbose > 2:
        logger.level = logging.INFO
    elif args.verbose > 1:
        logger.level = logging.WARNING
    else:
        logger.level = logging.ERROR

    return args

DEFAULT_FILEROOT='.'

def configure_fileroot(args):
    file_root = args.file_root

    # Default to top of git repo
    logger.info('Defaulting --file-root to top of git repo')
    if file_root is None:
        script_dir = os.path.dirname(__file__)
        file_root = os.path.normpath(script_dir + '/../')
        file_root = os.path.relpath(file_root)

    if not os.path.isdir(file_root):
        logger.warning('file-root %s does not exist', file_root)

    logger.info('Saving files to file-root %s', file_root)
    args.file_root = file_root

    return args

def parse_arguments():
    parser = argparse.ArgumentParser(description='scrape Shopify admin pages'
                ' that cannot be queried by API')

    parser.add_argument('-v', '--verbose', action='count', default=0)
    parser.add_argument('--cache-path', default='cache.json',
        help='JSON file caching cookies and credentials')
    parser.add_argument('--file-root',
        help='Directory to save template files into')
    parser.add_argument('--no-logout', action='store_true',
        help='Stay logged in at the end of this session')

    args = parser.parse_args()
    return args



if __name__ == '__main__':
    args = parse_arguments()
    args = configure_logger(args)
    args = configure_fileroot(args)

    sys.exit( main(args) )
