# Patch Firefox binary in standalone-firefox Docker image

# Table of Contents
* [Docker](#docker)
  * [List image tags](#docker-tags)
* [Mozilla source](#source)
  * [Fetch repository](#source-repo)
  * [Create patch](#source-patch)
  * [Build](#source-build)
* [Run Firefox in container](#docker)



# <a name='docker' />Docker


## <a name='docker-tags' />List image tags

```
> REPO_URL='https://registry.hub.docker.com/v2/repositories'
> curl -s ${REPO_URL}/library/ubuntu/tags?name=bionic  | jq .results[].name
> curl -s ${REPO_URL}/selenium/standalone-firefox/tags | jq .results[].name
```



# <a name='source' />Mozilla source


## <a name='source-repo' />Fetch repository

```
sudo apt install mercurial

cd ~/Dev/src/
hg clone https://hg.mozilla.org/mozilla-unified/
```


## <a name='source-patch' />Create patch

```
> hg tags
Find tag of desired release, e.g. FIREFOX_89_0_2_RELEASE
> hg update -r TAG_OF_RELEASE
Edit files involving webdriver handling of cookie domain
> hg commit
> hg export -r . -o /tmp/mozilla.patch
```


## <a name='build-source' />Build Firefox from source

```
> docker build --target firefox --tag makersfabric/firefox:89.0.2 .
```



# <a name='docker' />Run Firefox in container

See
* https://stackoverflow.com/questions/16296753/can-you-run-gui-applications-in-a-docker-container

```
> docker run -it --rm -v $XSOCK:$XSOCK -v $XAUTH:$XAUTH -e XAUTHORITY=$XAUTH \
    -e DISPLAY=$DISPLAY --entrypoint /opt/firefox/firefox \
    makersfabric/firefox:89.0.2
```
