# Exported files for Shopify Admin > Settings > Notifications

Table of Contents
* [Selenium](#selenium)
* [Script](#script)



<a name='selenium' />Selenium

* Bring up Selenium server running Firefox

```
> (cd selenium-firefox && sudo docker-compose up -d)
```



<a name='script' />Script

* Install python dependencies in virtualenv

```
> mkvirtualenv --python=python3 scrape-shopify
> pip install -r bin/requirements.py.txt
```

* Activate virtualenv

```
> workon scrape-shopify
```

* Execute script to download notifications with current directory as root

```
> python bin/scrape-shopify.py -v
```
